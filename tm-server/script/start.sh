!#/usr/bin/env bash

if [ -f ./tm-server.pid ]; then
	echo "Task-manager server already started"
	exit 1;
fi

mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -jar ../../tm-server.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "Task-manager is running with pid "$!