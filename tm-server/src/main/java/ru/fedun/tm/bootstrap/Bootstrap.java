package ru.fedun.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.*;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.repository.ISessionRepository;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.*;
import ru.fedun.tm.endpoint.*;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.SessionRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @Getter
    @NotNull
    private final ILoadService loadService = new LoadService(this);

    @Getter
    @NotNull
    private final ISqlConnectionService sqlConnectionService = new SqlConnectionService(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private void registry(final @NotNull Object endpoint) {
        final @NotNull String host = propertyService.getServerHost();
        final @NotNull Integer port = propertyService.getServerPort();
        final @NotNull String name = endpoint.getClass().getSimpleName();
        final @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        registry(adminEndpoint);
        registry(dataEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void initProps() throws Exception {
        propertyService.init();
    }

    public void run() throws Exception {
        initProps();
        initEndpoint();
        System.out.println("WELCOME TO TASK MANAGER");
    }

}
