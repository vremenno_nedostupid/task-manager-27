package ru.fedun.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.entity.Project;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @Nullable
    private String userId;

    @NotNull
    private Date startDate;

    @NotNull
    private Date completeDate;

    @NotNull
    private Date creationDate;

    public ProjectDTO(@NotNull final Project project) {
        id = project.getId();
        name = project.getName();
        description = project.getDescription();
        userId = project.getUser().getId();
        creationDate = project.getCreationDate();
        startDate = project.getStartDate();
        completeDate = project.getCompleteDate();
    }

    @NotNull
    public static ProjectDTO toDTO(@NotNull final Project project) {
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(@NotNull final Collection<Project> projects) {
        if (projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            result.add(new ProjectDTO(project));
        }
        return result;
    }

    @Override
    public String toString() {
        return getName();
    }

}
