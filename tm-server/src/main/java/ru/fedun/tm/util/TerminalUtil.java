package ru.fedun.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        @NotNull String value = "";
        while (value.isEmpty()) {
            value = SCANNER.nextLine();
        }
        return value;
    }

    @NotNull
    @SneakyThrows
    static Integer nextInt() {
        @Nullable final String value = nextLine();
        return Integer.parseInt(value);
    }

}
