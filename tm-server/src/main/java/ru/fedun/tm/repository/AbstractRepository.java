package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IRepository;
import ru.fedun.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final E e) {
        entityManager.persist(e);
    }

    public void merge(@NotNull final E e) {
        entityManager.merge(e);
    }

    public void remove(@NotNull final E e) {
        entityManager.remove(e);
    }

}
