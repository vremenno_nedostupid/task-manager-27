package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.IProjectEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint implements IProjectEndpoint {

    private ServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().createProject(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAllByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> showAllProjects(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO showProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findOneById(session.getUserId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO showProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findOneByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

}
