package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.ITaskEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.TaskDTO;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    private ServiceLocator serviceLocator;

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @NotNull final String taskName,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().createTask(sessionDTO.getUserId(), taskName, projectName, description);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskStartDate(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "date", partName = "date") @NotNull final Date date
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateStartDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateTaskCompleteDate(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "date", partName = "date") @NotNull final Date date
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateCompleteDate(sessionDTO.getUserId(), id, date);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countAllTasks(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getTaskService().countAllTasks();
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserTasks(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().countByUserId(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Long countProjectTasks(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().countByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().countByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return TaskDTO.toDTO(serviceLocator.getTaskService().findOneById(sessionDTO.getUserId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return TaskDTO.toDTO(serviceLocator.getTaskService().findOneByName(sessionDTO.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getTaskService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAllByUserId(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAllByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAllByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeOneById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeOneByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @WebMethod
    public void clearTasksByUserId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void clearTasksByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAllByProjectId(projectId);
    }

    @Override
    @WebMethod
    public void clearUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAllByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

}
