package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.fedun.tm.exception.incorrect.IncorrectStartDateException;
import ru.fedun.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@NotNull Project project) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
    }

    @Override
    public void merge(@NotNull Project project) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull Project project) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void createProject(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(serviceLocator.getUserService().findOneById(userId));
        persist(project);
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = findOneById(userId, id);
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return;
        merge(project);
    }

    @Override
    public void updateStartDate(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = findOneById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException();
        project.setStartDate(date);
        merge(project);
    }

    @Override
    public void updateCompleteDate(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = findOneById(userId, id);
        if (project.getStartDate().after(date)) throw new IncorrectCompleteDateException();
        project.setCompleteDate(date);
        merge(project);
    }

    @NotNull
    @Override
    public Long countAllProjects() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.count();
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Long countUserProjects(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.countByUserId(userId);
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneById(userId, id);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTitleException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneByName(userId, name);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAll());
        entityManager.close();
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAllByUserId(userId));
        entityManager.close();
        return projectsDTO;
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTitleException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
