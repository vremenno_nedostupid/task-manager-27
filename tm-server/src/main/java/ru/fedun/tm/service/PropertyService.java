package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    private final String file = "/application.properties";

    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        InputStream input = PropertyService.class.getResourceAsStream(file);
        properties.load(input);
    }


    @NotNull
    @Override
    public String getServerHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }


    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

}
