package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ILoadService getLoadService();

    @NotNull
    ISqlConnectionService getSqlConnectionService();

}
