package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.SessionDTO;

public interface IDataEndpoint {

    void saveXmlByJaxb(@NotNull SessionDTO session) throws Exception;

    void loadXmlByJaxb(@NotNull SessionDTO session) throws Exception;

    void clearXmlFileJaxb(@NotNull SessionDTO session) throws Exception;

    void saveXmlByFasterXml(@NotNull SessionDTO session) throws Exception;

    void loadXmlByFasterXml(@NotNull SessionDTO session) throws Exception;

    void clearXmlFileFasterXml(@NotNull SessionDTO session) throws Exception;

    void saveJsonByJaxb(@NotNull SessionDTO session) throws Exception;

    void loadJsonByJaxb(@NotNull SessionDTO session) throws Exception;

    void clearJsonFileJaxb(@NotNull SessionDTO session) throws Exception;

    void saveJsonByFasterXml(@NotNull SessionDTO session) throws Exception;

    void loadJsonByFasterXml(@NotNull SessionDTO session) throws Exception;

    void clearJsonFileFasterXml(@NotNull SessionDTO session) throws Exception;

    void saveBinary(@NotNull SessionDTO session) throws Exception;

    void loadBinary(@NotNull SessionDTO session) throws Exception;

    void clearBinaryFile(@NotNull SessionDTO session) throws Exception;

    void saveBase64(@NotNull SessionDTO session) throws Exception;

    void loadBase64(@NotNull SessionDTO session) throws Exception;

    void clearBase64File(@NotNull SessionDTO session) throws Exception;

}
