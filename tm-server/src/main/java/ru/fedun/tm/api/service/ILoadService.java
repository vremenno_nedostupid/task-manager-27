package ru.fedun.tm.api.service;

public interface ILoadService {

    void saveXmlByJaxb() throws Exception;

    void loadXmlByJaxb() throws Exception;

    void clearXmlFileJaxb() throws Exception;

    void saveXmlByFasterXml() throws Exception;

    void loadXmlByFasterXml() throws Exception;

    void clearXmlFileFasterXml() throws Exception;

    void saveJsonByJaxb() throws Exception;

    void loadJsonByJaxb() throws Exception;

    void clearJsonFileJaxb() throws Exception;

    void saveJsonByFasterXml() throws Exception;

    void loadJsonByFasterXml() throws Exception;

    void clearJsonFileFasterXml() throws Exception;

    void saveBinary() throws Exception;

    void loadBinary() throws Exception;

    void clearBinaryFile() throws Exception;

    void saveBase64() throws Exception;

    void loadBase64() throws Exception;

    void clearBase64File() throws Exception;

}
