package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    Long count();

    @NotNull
    Long countByUserId(@NotNull String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

}
