package ru.fedun.tm.exception.incorrect;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class IncorrectBehaviourException extends AbstractRuntimeException {

    private final static String message = "Oops, something went wrong...";

    public IncorrectBehaviourException() {
        super(message);
    }
}
