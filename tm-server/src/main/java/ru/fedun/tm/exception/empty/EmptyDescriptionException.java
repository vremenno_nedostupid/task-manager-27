package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public final class EmptyDescriptionException extends AbstractRuntimeException {

    private final static String message = "Error! Empty command...";

    public EmptyDescriptionException() {
        super(message);
    }

}
