package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyLastNameException extends AbstractRuntimeException {

    private static final String message = "Error! Last name is empty...";

    public EmptyLastNameException() {
        super(message);
    }

}
