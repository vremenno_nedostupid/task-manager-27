package ru.fedun.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.api.service.IDomainService;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(UnitServerCategory.class)
public class DomainServiceTest {
/*

    private IDomainService domainService;
    private ITaskService taskService;
    private IProjectService projectService;
    private IUserService userService;


    @Before
    public void initService() {
        taskService = new TaskService(new TaskRepository());
        projectService = new ProjectService(new ProjectRepository());
        userService = new UserService(new UserRepository());
        domainService = new DomainService(taskService, projectService, userService);
    }

    @Test
    public void loadTest() {
        final Domain domain = new Domain();
        domain.setTasks(Collections.singletonList(new Task()));
        domain.setProjects(Collections.singletonList(new ProjectDTO()));
        domain.setUsers(Collections.singletonList(new User()));
        assertTrue(taskService.getEntity().isEmpty());
        assertTrue(projectService.getEntity().isEmpty());
        assertTrue(userService.getEntity().isEmpty());
        domainService.load(domain);
        assertFalse(taskService.getEntity().isEmpty());
        assertFalse(projectService.getEntity().isEmpty());
        assertFalse(userService.getEntity().isEmpty());
    }

    @Test
    public void exportTest() {
        final Domain domain = new Domain();
        taskService.create("123", "task");
        projectService.create("123", "project");
        userService.create("user", "pass", "Ivan", "Loginov");
        assertTrue(domain.getTasks().isEmpty());
        assertTrue(domain.getProjects().isEmpty());
        assertTrue(domain.getUsers().isEmpty());
        domainService.export(domain);
        assertFalse(domain.getTasks().isEmpty());
        assertFalse(domain.getProjects().isEmpty());
        assertFalse(domain.getUsers().isEmpty());
    }
*/

}
