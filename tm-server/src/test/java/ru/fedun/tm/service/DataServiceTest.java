package ru.fedun.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.marker.UnitServerCategory;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(UnitServerCategory.class)
public class DataServiceTest {
/*
    private final Bootstrap bootstrap = new Bootstrap();

    @Before
    public void init() {
        bootstrap.getUserService().create("user", "pass", "Ivan", "Ivanov", "email@.com");
        bootstrap.getProjectService().create("123", "project", "description");
        bootstrap.getTaskService().create("123", "task", "description");
    }

    public void clearData() {
        bootstrap.getUserService().clear();
        bootstrap.getProjectService().clear();
        bootstrap.getTaskService().clear();
    }

    public boolean checkEntityLists() {
        if (!bootstrap.getUserService().getEntity().isEmpty()) return false;
        if (!bootstrap.getProjectService().getEntity().isEmpty()) return false;
        return bootstrap.getTaskService().getEntity().isEmpty();
    }


    @Test
    public void base64Test() throws Exception {
        bootstrap.getLoadService().saveBase64();
        final File file = new File(DataConstant.FILE_BASE64);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadBase64();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearBase64File();
        assertFalse(file.exists());
    }


    @Test
    public void binaryTest() throws Exception {
        bootstrap.getLoadService().saveBinary();
        final File file = new File(DataConstant.FILE_BINARY);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadBinary();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearBinaryFile();
        assertFalse(file.exists());
    }


    @Test
    public void jsonFasterxmlTest() throws Exception {
        bootstrap.getLoadService().saveJsonByFasterXml();
        final File file = new File(DataConstant.FILE_JSON_FX);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadJsonByFasterXml();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearJsonFileFasterXml();
        assertFalse(file.exists());
    }


    @Test
    public void xmlFasterxmlTest() throws Exception {
        bootstrap.getLoadService().saveXmlByFasterXml();
        final File file = new File(DataConstant.FILE_XML_FX);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadXmlByFasterXml();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearXmlFileFasterXml();
        assertFalse(file.exists());
    }


    @Test
    public void jsonJaxbTest() throws Exception {
        bootstrap.getLoadService().saveJsonByJaxb();
        final File file = new File(DataConstant.FILE_JSON_JB);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadJsonByJaxb();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearJsonFileJaxb();
        assertFalse(file.exists());
    }


    @Test
    public void xmlJaxbTest() throws Exception {
        bootstrap.getLoadService().saveXmlByJaxb();
        final File file = new File(DataConstant.FILE_XML_JB);
        assertTrue(file.exists());
        clearData();
        assertTrue(checkEntityLists());
        bootstrap.getLoadService().loadXmlByJaxb();
        assertFalse(checkEntityLists());
        bootstrap.getLoadService().clearXmlFileJaxb();
        assertFalse(file.exists());
    }*/

}
