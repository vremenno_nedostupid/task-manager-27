package ru.fedun.tm.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class SessionServiceTest {
/*

    private final Bootstrap bootstrap = new Bootstrap();
    private SessionService sessionService;

    @Before
    public void initService() throws Exception {
        sessionService = new SessionService(new SessionRepository(), bootstrap);
        bootstrap.getPropertyService().init();
        bootstrap.getUserService().create("user", "test", "Ivan", "Sergiy", "Email");
        bootstrap.getUserService().create("user", "test", "Ivan", "Sergiy", Role.USER);
    }

    @After
    public void removeUser() {
        bootstrap.getUserService().removeByLogin("user");
    }

    @Test
    public void checkDataAccessTest() {
        assertTrue(sessionService.checkDataAccess("user", "test"));
    }

    @Test
    public void openTest() {
        assertNotNull(sessionService.open("user", "test"));
    }

    @Test
    public void signTest() {
        final SessionDTO session = new SessionDTO();
        assertNotNull(sessionService.sign(session));
    }

    @Test
    public void getUserTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        assertNotNull(sessionService.getUser(session));

    }

    @Test
    public void getUserIdTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        assertNotNull(sessionService.getUserId(session));
    }

    @Test
    public void getListSessionTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        assertNotNull(sessionService.getSessionList(session));
    }

    @Test
    public void closeTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.close(session);
        assertFalse(sessionService.getEntity().contains(session));
    }

    @Test
    public void closeAllTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.closeAll(session);
        assertTrue(sessionService.getEntity().isEmpty());
    }

    @Test
    public void isValidTest() {
        final SessionDTO session = sessionService.open("user", "test");
        assertTrue(sessionService.isValid(session));
    }

    @Test
    public void validateTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTestSessionAccessDeniedException() throws Exception {
        sessionService.validate(new SessionDTO());
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTestSignatureAccessDeniedException() throws Exception {
        final SessionDTO session = new SessionDTO();
        session.setStartTime(System.currentTimeMillis());
        session.setUserId("123");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTestUserIdAccessDeniedException() throws Exception {
        final SessionDTO session = new SessionDTO();
        session.setStartTime(System.currentTimeMillis());
        session.setSignature("123asd");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTestTimestampAccessDeniedException() throws Exception {
        final SessionDTO session = new SessionDTO();
        session.setSignature("123asd");
        session.setUserId("123");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTestContainsAccessDeniedException() throws Exception {
        final SessionDTO session = new SessionDTO();
        session.setSignature("123asd");
        session.setUserId("123");
        session.setStartTime(System.currentTimeMillis());
        sessionService.validate(session);
    }

    @Test
    public void validateWithRoleTest() throws Exception {
        final User user = bootstrap.getUserService().findByLogin("user");
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.validate(session, user.getRole());
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithRoleTestAccessDeniedException() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.validate(session, Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        sessionService.signOutByLogin("user");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByUserIdTest() throws Exception {
        final SessionDTO session = sessionService.open("user", "test");
        final User user = sessionService.getUser(session);
        final String userId = user.getId();
        sessionService.signOutByUserId(userId);
        sessionService.validate(session);
    }

    @Test
    public void getListTest() {
        sessionService.open("user", "test");
        assertNotNull(sessionService.getEntity());
    }

    @Test
    public void loadVarargsTest() {
        final SessionDTO session1 = new SessionDTO();
        final SessionDTO session2 = new SessionDTO();
        sessionService.load(session1, session2);
        assertNotNull(sessionService.getEntity());
    }

    @Test
    public void loadListTest() {
        final List<SessionDTO> sessions = new ArrayList<>();
        final SessionDTO session1 = new SessionDTO();
        sessions.add(session1);
        final SessionDTO session2 = new SessionDTO();
        sessions.add(session2);
        sessionService.load(sessions);
        assertNotNull(sessionService.getEntity());
    }

    @Test
    public void clearWithoutUserIdTest() {
        final SessionDTO session1 = new SessionDTO();
        final SessionDTO session2 = new SessionDTO();
        sessionService.load(session1, session2);
        sessionService.clear();
        assertTrue(sessionService.getEntity().isEmpty());
    }
*/

}
