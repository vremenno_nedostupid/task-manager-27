package ru.fedun.tm.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.marker.UnitServerCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class TaskRepositoryTest {
/*
    final TaskRepository taskRepository = new TaskRepository();

    @Before
    public void init() {
        final Task task1 = new Task();
        task1.setUserId("001");
        task1.setTitle("Task 1");
        task1.setDescription("Description 1");
        taskRepository.add(task1);

        final Task task2 = new Task();
        task2.setUserId("002");
        task2.setTitle("Task 2");
        task2.setDescription("Description 2");
        taskRepository.add(task2);

        final Task task3 = new Task();
        task3.setUserId("002");
        task3.setTitle("Task 3");
        task3.setDescription("Description 3");
        taskRepository.add(task3);
    }

    @Test
    public void addTest() {
        taskRepository.clear();
        final Task task1 = new Task();
        task1.setUserId("001");
        task1.setTitle("Task 1");
        task1.setDescription("Description 1");
        taskRepository.add(task1);
        assertSame(taskRepository.getEntities().get(0), task1);
    }

    @Test
    public void removeTest() {
        final Task task = taskRepository.getEntities().get(0);
        taskRepository.remove(task.getUserId(), task);
        assertFalse(taskRepository.getEntities().contains(task));
    }

    @Test
    public void findAllTest() {
        assertEquals(taskRepository.findAll().size(), 3);
    }

    @Test
    public void clearTest() {
        taskRepository.clear();
        assertEquals(taskRepository.findAll().size(), 0);
    }

    @Test
    public void findOneByIdTest() {
        final Task task = taskRepository.getEntities().get(0);
        final String taskId = task.getId();
        assertEquals(taskRepository.findOneById("001", taskId), task);
    }

    @Test
    public void findOneByIndexTest() {
        final Task task = taskRepository.getEntities().get(0);
        assertEquals(taskRepository.findOneByIndex("001", 0), task);
    }

    @Test
    public void findOneByNameTest() {
        final Task task = taskRepository.getEntities().get(0);
        final String taskName = task.getTitle();
        assertEquals(taskRepository.findOneByTitle("001", taskName), task);
    }

    @Test
    public void removeOneByIdTest() {
        final Task task = taskRepository.getEntities().get(0);
        final String taskId = task.getId();
        taskRepository.removeOneById(task.getUserId(), taskId);
        assertFalse(taskRepository.getEntities().contains(task));
    }

    @Test
    public void removeOneByIndexTest() {
        final Task task = taskRepository.getEntities().get(0);
        taskRepository.removeOneByIndex(task.getUserId(), 0);
        assertFalse(taskRepository.getEntities().contains(task));
    }

    @Test
    public void removeOneByNameTest() {
        final Task task = taskRepository.getEntities().get(0);
        final String taskName = task.getTitle();
        taskRepository.removeOneByTitle(task.getUserId(), taskName);
        assertFalse(taskRepository.getEntities().contains(task));
    }

    @Test
    public void clearEntitiesTest() {
        assertEquals(taskRepository.getEntities().size(), 3);
        taskRepository.clear();
        assertEquals(taskRepository.getEntities().size(), 0);
    }

    @Test
    public void addVarargsTest() {
        taskRepository.clear();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        taskRepository.add(task1, task2, task3);
        assertEquals(taskRepository.getEntities().size(), 3);
    }

    @Test
    public void AddListTest() {
        taskRepository.clear();
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        taskRepository.add(tasks);
        assertEquals(taskRepository.getEntities().size(), 3);
    }

    @Test
    public void loadOneTest() {
        assertEquals(taskRepository.getEntities().size(), 3);
        final Task task = new Task();
        taskRepository.load(task);
        assertEquals(taskRepository.getEntities().size(), 1);
    }

    @Test
    public void loadVarargsTest() {
        assertEquals(taskRepository.getEntities().size(), 3);
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskRepository.load(task1, task2);
        assertEquals(taskRepository.getEntities().size(), 2);
    }

    @Test
    public void loadListTest() {
        assertEquals(taskRepository.getEntities().size(), 3);
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.load(tasks);
        assertEquals(taskRepository.getEntities().size(), 2);
    }*/

}
