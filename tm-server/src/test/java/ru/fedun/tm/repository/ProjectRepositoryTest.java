package ru.fedun.tm.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.marker.UnitServerCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class ProjectRepositoryTest {
/*

    final ProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void init() {
        final ProjectDTO project1 = new ProjectDTO();
        project1.setUserId("001");
        project1.setTitle("Project 1");
        project1.setDescription("Description 1");
        projectRepository.add(project1);

        final ProjectDTO project2 = new ProjectDTO();
        project2.setUserId("002");
        project2.setTitle("Project 2");
        project2.setDescription("Description 2");
        projectRepository.add(project2);

        final ProjectDTO project3 = new ProjectDTO();
        project3.setUserId("002");
        project3.setTitle("Project 3");
        project3.setDescription("Description 3");
        projectRepository.add(project3);
    }

    @Test
    public void addTest() {
        projectRepository.clear();
        final ProjectDTO project1 = new ProjectDTO();
        project1.setUserId("001");
        project1.setTitle("Project 1");
        project1.setDescription("Description 1");
        projectRepository.add(project1);
        assertSame(projectRepository.getEntities().get(0), project1);
    }

    @Test
    public void removeTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        projectRepository.remove(project.getUserId(), project);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void findAllTest() {
        assertEquals(projectRepository.findAll().size(), 3);
    }

    @Test
    public void clearTest() {
        projectRepository.clear();
        assertEquals(projectRepository.findAll().size(), 0);
    }

    @Test
    public void findOneByIdTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        final String projectId = project.getId();
        assertEquals(projectRepository.findOneById("001", projectId), project);
    }

    @Test
    public void findOneByIndexTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        assertEquals(projectRepository.findOneByIndex("001", 0), project);
    }

    @Test
    public void findOneByNameTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        final String projectName = project.getTitle();
        assertEquals(projectRepository.findOneByTitle("001", projectName), project);
    }

    @Test
    public void removeOneByIdTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        final String projectId = project.getId();
        projectRepository.removeOneById(project.getUserId(), projectId);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void removeOneByIndexTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        projectRepository.removeOneByIndex(project.getUserId(), 0);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void removeOneByNameTest() {
        final ProjectDTO project = projectRepository.getEntities().get(0);
        final String projectName = project.getTitle();
        projectRepository.removeOneByTitle(project.getUserId(), projectName);
        assertFalse(projectRepository.getEntities().contains(project));
    }

    @Test
    public void clearEntitiesTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        projectRepository.clear();
        assertEquals(projectRepository.getEntities().size(), 0);
    }

    @Test
    public void addVarargsTest() {
        projectRepository.clear();
        final ProjectDTO project1 = new ProjectDTO();
        final ProjectDTO project2 = new ProjectDTO();
        final ProjectDTO project3 = new ProjectDTO();
        projectRepository.add(project1, project2, project3);
        assertEquals(projectRepository.getEntities().size(), 3);
    }

    @Test
    public void AddListTest() {
        projectRepository.clear();
        final List<ProjectDTO> projects = new ArrayList<>();
        final ProjectDTO project1 = new ProjectDTO();
        final ProjectDTO project2 = new ProjectDTO();
        final ProjectDTO project3 = new ProjectDTO();
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectRepository.add(projects);
        assertEquals(projectRepository.getEntities().size(), 3);
    }

    @Test
    public void loadOneTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final ProjectDTO project = new ProjectDTO();
        projectRepository.load(project);
        assertEquals(projectRepository.getEntities().size(), 1);
    }

    @Test
    public void loadVarargsTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final ProjectDTO project1 = new ProjectDTO();
        final ProjectDTO project2 = new ProjectDTO();
        projectRepository.load(project1, project2);
        assertEquals(projectRepository.getEntities().size(), 2);
    }

    @Test
    public void loadListTest() {
        assertEquals(projectRepository.getEntities().size(), 3);
        final List<ProjectDTO> projects = new ArrayList<>();
        final ProjectDTO project1 = new ProjectDTO();
        final ProjectDTO project2 = new ProjectDTO();
        projects.add(project1);
        projects.add(project2);
        projectRepository.load(projects);
        assertEquals(projectRepository.getEntities().size(), 2);
    }
*/

}
