package ru.fedun.tm.exception.incorrect;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class IncorrectPasswordException extends AbstractRuntimeException {

    private final static String message = "Error! Password is incorrect...";

    public IncorrectPasswordException() {
        super(message);
    }
}
