package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
