package ru.fedun.tm.constant;

public class DataConstant {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_XML_FX = "./data_fx.xml";

    public static final String FILE_JSON_FX = "./data_fx.json";

    public static final String FILE_XML_JB = "./data_jb.xml";

    public static final String FILE_JSON_JB = "./data_jb.json";

}

