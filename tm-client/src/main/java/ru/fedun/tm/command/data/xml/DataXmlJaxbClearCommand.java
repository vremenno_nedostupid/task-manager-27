package ru.fedun.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataXmlJaxbClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-xml-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from xml (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) CLEAR]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().clearXmlFileJaxb(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
