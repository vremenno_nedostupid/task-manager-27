package ru.fedun.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.endpoint.User;
import ru.fedun.tm.endpoint.UserDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class ProfileDeleteCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "delete-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete profile.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DELETE PROFILE]");
        System.out.println("DO YOU REALLY WANT DELETE PROFILE?");
        System.out.println("1 - YES, 2 - NO");
        @NotNull final Integer answer = TerminalUtil.nextInt();
        if (answer.equals(1)) {
            @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
            @NotNull final UserDTO user = serviceLocator.getUserEndpoint().showUserProfile(session);
            serviceLocator.getAdminEndpoint().removeUserByLogin(session, user.getLogin());
        } else return;
        System.out.println("[OK]");
        System.out.println();
    }

}
