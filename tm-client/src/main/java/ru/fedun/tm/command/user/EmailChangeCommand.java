package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class EmailChangeCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-email";
    }

    @NotNull
    @Override
    public String description() {
        return "Change profile e-mail.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE EMAIL]");
        System.out.println("[ENTER NEW EMAIL:]");
        @Nullable final String newEmail = TerminalUtil.nextLine();
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getUserEndpoint().updateMail(session, newEmail);
        System.out.println("[OK]");
        System.out.println();
    }

}
