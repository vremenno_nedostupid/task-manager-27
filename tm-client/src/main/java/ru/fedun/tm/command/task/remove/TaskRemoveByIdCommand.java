package ru.fedun.tm.command.task.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by_id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        if (id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        serviceLocator.getTaskEndpoint().removeTaskById(session, id);
        System.out.println("[OK]");
        System.out.println();
    }

}
