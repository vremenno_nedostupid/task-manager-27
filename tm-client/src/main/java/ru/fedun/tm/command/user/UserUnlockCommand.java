package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user in system.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER USER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminEndpoint().unlockUserByLogin(session, login);
        System.out.println("[OK]");
        System.out.println();
    }

}
