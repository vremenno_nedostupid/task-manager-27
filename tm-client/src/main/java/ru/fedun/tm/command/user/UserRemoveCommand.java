package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Exception_Exception;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete user from system.";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER USER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminEndpoint().removeUserByLogin(session, login);
        System.out.println("[OK]");
        System.out.println();
    }

}
