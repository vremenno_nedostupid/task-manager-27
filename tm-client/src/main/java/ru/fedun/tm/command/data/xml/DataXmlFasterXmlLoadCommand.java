package ru.fedun.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-xml-fx";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data to xml (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (FASTERXML) LOAD]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().loadXmlByFasterXml(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
