package ru.fedun.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-01T21:48:39.374+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.fedun.ru/", name = "AdminEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/unlockUserByLoginResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/unlockUserByLogin/Fault/Exception")})
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.UnlockUserByLoginResponse")
    public void unlockUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.fedun.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithRoleRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithRoleResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithRole/Fault/Exception")})
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.CreateUserWithRoleResponse")
    public void createUserWithRole(
        @WebParam(name = "session", targetNamespace = "")
        ru.fedun.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email,
        @WebParam(name = "role", targetNamespace = "")
        ru.fedun.tm.endpoint.Role role
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithEmailRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithEmailResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/createUserWithEmail/Fault/Exception")})
    @RequestWrapper(localName = "createUserWithEmail", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.CreateUserWithEmail")
    @ResponseWrapper(localName = "createUserWithEmailResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.CreateUserWithEmailResponse")
    public void createUserWithEmail(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/lockUserByLoginResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/lockUserByLogin/Fault/Exception")})
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.LockUserByLoginResponse")
    public void lockUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.fedun.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserByLoginRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserByLoginResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserByLogin/Fault/Exception")})
    @RequestWrapper(localName = "removeUserByLogin", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.RemoveUserByLogin")
    @ResponseWrapper(localName = "removeUserByLoginResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.RemoveUserByLoginResponse")
    public void removeUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.fedun.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserByIdRequest", output = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserByIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.fedun.ru/AdminEndpoint/removeUserById/Fault/Exception")})
    @RequestWrapper(localName = "removeUserById", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.RemoveUserById")
    @ResponseWrapper(localName = "removeUserByIdResponse", targetNamespace = "http://endpoint.tm.fedun.ru/", className = "ru.fedun.tm.endpoint.RemoveUserByIdResponse")
    public void removeUserById(
        @WebParam(name = "session", targetNamespace = "")
        ru.fedun.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    ) throws Exception_Exception;
}
