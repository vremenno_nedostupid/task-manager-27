package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.service.ISessionService;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class SessionService implements ISessionService {

    private SessionDTO currentSession;

    @NotNull
    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(@NotNull final SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }

}
