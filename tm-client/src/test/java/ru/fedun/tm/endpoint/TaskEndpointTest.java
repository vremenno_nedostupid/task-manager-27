package ru.fedun.tm.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.marker.IntegrationClientCategory;

@Category(IntegrationClientCategory.class)
public class TaskEndpointTest {
/*

    private final Bootstrap bootstrap = new Bootstrap();
    private Session userSession;

    @Before
    public void initAdminSession() throws Exception_Exception {
        userSession = bootstrap.getSessionEndpoint().openSession("test", "test");
        bootstrap.getTaskEndpoint().createTask(userSession, "task", "test");
    }

    @After
    public void clearData() throws Exception_Exception {
        bootstrap.getTaskEndpoint().clearTasks(userSession);
    }

    @Test
    public void createTask() throws Exception_Exception {
        bootstrap.getTaskEndpoint().createTask(userSession, "task2", "test");
        Assert.assertNotNull(bootstrap.getTaskEndpoint().showTaskByName(userSession, "task2"));
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task2");
    }

    @Test
    public void showAllTest() throws Exception_Exception {
        Assert.assertNotNull(bootstrap.getTaskEndpoint().showAllTasks(userSession));
    }

    @Test
    public void clearTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
        bootstrap.getTaskEndpoint().clearTasks(userSession);
        Assert.assertTrue(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
    }

    @Test
    public void showOneByIdTest() throws Exception_Exception {
        final Task task = bootstrap.getTaskEndpoint().showTaskByName(userSession, "task");
        Assert.assertEquals(task.getTitle(), bootstrap.getTaskEndpoint().showTaskById(userSession, task.getId()).getTitle());
    }

    @Test
    public void showOneByIndexTest() throws Exception_Exception {
        final Task task = bootstrap.getTaskEndpoint().showTaskByName(userSession, "task");
        Assert.assertEquals(task.getTitle(), bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0).getTitle());
    }

    @Test
    public void showOneByNameTest() throws Exception_Exception {
        final Task task = bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0);
        Assert.assertEquals(task.getTitle(), bootstrap.getTaskEndpoint().showTaskByName(userSession, "task").getTitle());
    }

    @Test
    public void removeOneByIdTest() throws Exception_Exception {
        final String taskId = bootstrap.getTaskEndpoint().showAllTasks(userSession).get(0).getId();
        Assert.assertFalse(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
        bootstrap.getTaskEndpoint().removeTaskById(userSession, taskId);
        Assert.assertTrue(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
    }

    @Test
    public void removeOneByIndexTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
        bootstrap.getTaskEndpoint().removeTaskByIndex(userSession, 0);
        Assert.assertTrue(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
    }

    @Test
    public void removeOneByNameTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        Assert.assertTrue(bootstrap.getTaskEndpoint().showAllTasks(userSession).isEmpty());
    }

    @Test
    public void updateTaskByIdTest() throws Exception_Exception {
        final String taskId = bootstrap.getTaskEndpoint().showAllTasks(userSession).get(0).getId();
        final String name = "newTaskName";
        final String description = "newTaskDescription";
        bootstrap.getTaskEndpoint().updateTaskById(userSession, taskId, name, description);
        Assert.assertEquals(bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0).getTitle(), name);
        Assert.assertEquals(bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0).getDescription(), description);
    }

    @Test
    public void updateTaskByIndexTest() throws Exception_Exception {
        final String name = "newTaskName";
        final String description = "newTaskDescription";
        bootstrap.getTaskEndpoint().updateTaskByIndex(userSession, 0, name, description);
        Assert.assertEquals(bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0).getTitle(), name);
        Assert.assertEquals(bootstrap.getTaskEndpoint().showTaskByIndex(userSession, 0).getDescription(), description);
    }
*/

}
