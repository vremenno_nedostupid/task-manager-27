package ru.fedun.tm.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.marker.IntegrationClientCategory;

import javax.xml.ws.WebServiceException;

@Category(IntegrationClientCategory.class)
public class AdminEndpointTest {
/*
    private final Bootstrap bootstrap = new Bootstrap();
    private Session adminSession;
    private Session userSession;

    @Before
    public void initAdminSession() throws Exception_Exception {
        adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        userSession = bootstrap.getSessionEndpoint().openSession("test", "test");
    }

    @Test
    public void createWithRoleTest() throws Exception_Exception {
        bootstrap.getAdminEndpoint().createUserWithRole(adminSession, "newAdmin", "Sergei", "Pien", "Pass", Role.ADMIN);
        final Session session = bootstrap.getSessionEndpoint().openSession("newAdmin", "Pass");
        Assert.assertNotNull(session);
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getAdminEndpoint().removeUserByLogin(adminSession, "newAdmin");
    }

    @Test
    public void lockUserByLoginTest() throws Exception_Exception {
        bootstrap.getAdminEndpoint().lockUserByLogin(adminSession, "test");
        Assert.assertTrue(bootstrap.getUserEndpoint().showUserProfile(userSession).isLocked());
    }

    @Test
    public void unlockUserByLoginTest() throws Exception_Exception {
        bootstrap.getAdminEndpoint().lockUserByLogin(adminSession, "test");
        Assert.assertTrue(bootstrap.getUserEndpoint().showUserProfile(userSession).isLocked());
        bootstrap.getAdminEndpoint().unlockUserByLogin(adminSession, "test");
        Assert.assertFalse(bootstrap.getUserEndpoint().showUserProfile(userSession).isLocked());
    }

    @Test(expected = WebServiceException.class)
    public void removeByIdTest() throws Exception_Exception {
        bootstrap.getAdminEndpoint().createUserWithEmail("newUser", "pass", "Roman", "Tomov", "email");
        final Session session = bootstrap.getSessionEndpoint().openSession("newUser", "pass");
        final String userId = bootstrap.getUserEndpoint().showUserProfile(session).getId();
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getAdminEndpoint().removeUserById(adminSession, userId);
        bootstrap.getSessionEndpoint().openSession("newUser", "pass");
    }

    @Test(expected = WebServiceException.class)
    public void removeByLoginTest() throws Exception_Exception {
        bootstrap.getAdminEndpoint().createUserWithEmail("newUser", "pass", "Roman", "Tom", "email");
        bootstrap.getAdminEndpoint().removeUserByLogin(adminSession, "newUser");
        bootstrap.getSessionEndpoint().openSession("newUser", "pass");
    }*/

}

