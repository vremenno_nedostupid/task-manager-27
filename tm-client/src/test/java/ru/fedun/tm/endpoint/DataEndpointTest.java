package ru.fedun.tm.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.marker.IntegrationClientCategory;

import java.io.File;

@Category(IntegrationClientCategory.class)
public class DataEndpointTest {
/*

    private final Bootstrap bootstrap = new Bootstrap();
    private Session adminSession;
    private Session userSession;

    @Before
    public void initAdminSession() throws Exception_Exception {
        adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        userSession = bootstrap.getSessionEndpoint().openSession("test", "test");
        bootstrap.getProjectEndpoint().createProject(userSession, "project", "test");
        bootstrap.getTaskEndpoint().createTask(userSession, "task", "test");
    }

    @After
    public void clearData() throws Exception_Exception {
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getSessionEndpoint().closeSession(userSession);
        bootstrap.getSessionEndpoint().closeSession(adminSession);
    }

    @Test
    public void base64Test() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveBase64(adminSession);
        final File file = new File(DataConstant.FILE_BASE64);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadBase64(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().clearBase64File(adminSession);
        Assert.assertFalse(file.exists());
    }

    @Test
    public void binaryTest() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveBinary(adminSession);
        final File file = new File(DataConstant.FILE_BINARY);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadBinary(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().clearBinaryFile(adminSession);
        Assert.assertFalse(file.exists());
    }

    @Test
    public void jsonFasterxmlTest() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveJsonByFasterXml(adminSession);
        final File file = new File(DataConstant.FILE_JSON_FX);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadJsonByFasterXml(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().loadJsonByFasterXml(adminSession);
        Assert.assertFalse(file.exists());
    }

    @Test
    public void xmlFasterxmlTest() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveXmlByFasterXml(adminSession);
        final File file = new File(DataConstant.FILE_XML_FX);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadXmlByFasterXml(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().clearXmlFileFasterXml(adminSession);
        Assert.assertFalse(file.exists());
    }

    @Test
    public void jsonJaxbTest() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveJsonByJaxb(adminSession);
        final File file = new File(DataConstant.FILE_JSON_JB);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadJsonByJaxb(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().clearJsonFileJaxb(adminSession);
        Assert.assertFalse(file.exists());
    }

    @Test
    public void xmlJaxbTest() throws Exception_Exception {
        bootstrap.getDataEndpoint().saveXmlByJaxb(adminSession);
        final File file = new File(DataConstant.FILE_XML_JB);
        bootstrap.getTaskEndpoint().removeTaskByName(userSession, "task");
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        bootstrap.getDataEndpoint().loadXmlByJaxb(adminSession);
        Assert.assertNotNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getDataEndpoint().clearXmlFileJaxb(adminSession);
        Assert.assertFalse(file.exists());
    }
*/

}
