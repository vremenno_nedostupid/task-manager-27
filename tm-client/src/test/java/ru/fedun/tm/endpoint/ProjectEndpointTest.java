package ru.fedun.tm.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.marker.IntegrationClientCategory;

import java.util.List;

@Category(IntegrationClientCategory.class)
public class ProjectEndpointTest {
/*

    private final Bootstrap bootstrap = new Bootstrap();
    private Session userSession;

    @Before
    public void initAdminSession() throws Exception_Exception {
        userSession = bootstrap.getSessionEndpoint().openSession("test", "test");
        bootstrap.getProjectEndpoint().createProject(userSession, "project", "test");
    }

    @After
    public void clearData() throws Exception_Exception {
        bootstrap.getProjectEndpoint().clearProjects(userSession);
    }


    @Test
    public void createProject() throws Exception_Exception {
        bootstrap.getProjectEndpoint().createProject(userSession, "project2", "test");
        Assert.assertNotNull(bootstrap.getProjectEndpoint().showProjectByName(userSession, "project2"));
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project2");
    }

    @Test
    public void findAllTest() throws Exception_Exception {
        Assert.assertNotNull(bootstrap.getProjectEndpoint().showAllProjects(userSession));
    }

    @Test
    public void clearTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
        bootstrap.getProjectEndpoint().clearProjects(userSession);
        Assert.assertTrue(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
    }

    @Test
    public void findOneByIdTest() throws Exception_Exception {
        final Project project = bootstrap.getProjectEndpoint().showProjectByName(userSession, "project");
        Assert.assertEquals(project.getTitle(), bootstrap.getProjectEndpoint().showProjectById(userSession, project.getId()).getTitle());
    }

    @Test
    public void findOneByIndexTest() throws Exception_Exception {
        final Project project = bootstrap.getProjectEndpoint().showProjectByName(userSession, "project");
        Assert.assertEquals(project.getTitle(), bootstrap.getProjectEndpoint().showProjectByIndex(userSession, 0).getTitle());
    }

    @Test
    public void findOneByNameTest() throws Exception_Exception {
        final Project project = bootstrap.getProjectEndpoint().showProjectByName(userSession, "project");
        Assert.assertEquals(project.getTitle(), bootstrap.getProjectEndpoint().showProjectByName(userSession, "project").getTitle());
    }

    @Test
    public void removeOneByIdTest() throws Exception_Exception {
        final String projectId = bootstrap.getProjectEndpoint().showAllProjects(userSession).get(0).getId();
        final List<Project> list = bootstrap.getProjectEndpoint().showAllProjects(userSession);
        Assert.assertFalse(list.isEmpty());
        bootstrap.getProjectEndpoint().removeProjectById(userSession, projectId);
        Assert.assertTrue(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
    }

    @Test
    public void removeOneByIndexTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
        bootstrap.getProjectEndpoint().removeProjectByIndex(userSession, 0);
        Assert.assertTrue(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
    }

    @Test
    public void removeOneByNameTest() throws Exception_Exception {
        Assert.assertFalse(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
        bootstrap.getProjectEndpoint().removeProjectByName(userSession, "project");
        Assert.assertTrue(bootstrap.getProjectEndpoint().showAllProjects(userSession).isEmpty());
    }

    @Test
    public void updateProjectByIdTest() throws Exception_Exception {
        final String projectId = bootstrap.getProjectEndpoint().showAllProjects(userSession).get(0).getId();
        final String name = "newProjectName";
        final String description = "newProjectDescription";
        bootstrap.getProjectEndpoint().updateProjectById(userSession, projectId, name, description);
        Assert.assertEquals(bootstrap.getProjectEndpoint().showProjectByIndex(userSession, 0).getTitle(), name);
        Assert.assertEquals(bootstrap.getProjectEndpoint().showProjectByIndex(userSession, 0).getDescription(), description);
    }

    @Test
    public void updateProjectByIndexTest() throws Exception_Exception {
        final String name = "newProjectName";
        final String description = "newProjectDescription";
        bootstrap.getProjectEndpoint().updateProjectByIndex(userSession, 0, name, description);
        Assert.assertEquals(bootstrap.getProjectEndpoint().showProjectByIndex(userSession, 0).getTitle(), name);
        Assert.assertEquals(bootstrap.getProjectEndpoint().showProjectByIndex(userSession, 0).getDescription(), description);
    }
*/

}
